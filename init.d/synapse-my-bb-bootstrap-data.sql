INSERT INTO `application_zones` (`id`, `name`, `url`, `allow_create`, `allow_sandboxing`, `allow_rename`, `inserted_at`, `updated_at`, `secret`, `ide_url`, `meta_api_domain`, `can_use_replicator`)
VALUES
  (1, 'development', 'http://on-prem.betty-internal.docker', 1, 1, 1, '2014-08-15 11:22:13', '2016-10-24 17:32:53', '439d3d4ef083125', 'http://ide.betty.docker/app/:identifier/pages', 'meta-api.betty.docker', 1);

INSERT INTO `users` (`id`, `inserted_at`, `updated_at`, `email`, `name`, `cas_token`, `global_admin`, `read_admin`, `encrypted_password`, `phone`, `reset_password_token`, `reset_password_sent_at`, `last_sign_in_at`, `invitation_token`, `invitation_created_at`, `invitation_sent_at`, `invitation_accepted_at`, `invited_by_id`, `avatar`, `occupation`, `location`, `linkedin`, `website`)
VALUES
	(1, '2018-11-28 14:11:54', '2018-11-28 15:58:51', 'admin@example.com', 'Admin Blocks', 'c530c3f4079a09f7804259002307b5e5', 1, 0, '$2a$12$5dircsRVQierQIpEtrn0b.0YgVj9qAEUeYujNzzTxqJ4NQIjuJtOO', NULL, NULL, NULL, '2018-11-28 15:58:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, '2018-11-28 14:11:54', '2018-11-28 15:58:51', 'owner@bettyblocks.com', 'Betty Blocks', 'd444c3f4079a09f7804259002307b5e50c656d52', 1, 0, '$2a$12$5dircsRVQierQIpEtrn0b.0YgVj9qAEUeYujNzzTxqJ4NQIjuJtOO', NULL, NULL, NULL, '2018-11-28 15:58:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `organizations` (`id`, `name`, `logo`, `login_background_image`, `inserted_at`, `updated_at`, `allow_versioning`,`application_zone_id`)
VALUES
	(1, 'Betty Blocks', NULL, NULL, '2018-11-29 11:03:11', '2018-11-29 11:03:11', 1, 1);

INSERT INTO `applications` (`id`, `identifier`, `name`, `app_uuid`, `status`, `usage_level`, `url`, `logo`, `login_background_image`, `organization_id`, `application_zone_id`, `inserted_at`, `updated_at`, `root_app_id`, `merge_target_id`, `application_type`, `current_app_job_id`, `can_be_merged`)
VALUES
	(1, 'not-my-bb', 'Fake app for components', '95c73be26cc742b19cb590a449f2afab', 'disabled', 'test', '', NULL, NULL, 1, 1, '2018-11-29 14:57:16', '2018-11-29 14:57:16', NULL, NULL, NULL, NULL, 1),
	(2, 'my-bb', 'Manage all apps', '11111111111111111111111111111111', 'enabled', 'test', 'http://my.betty.docker', NULL, NULL, 1, 1, '2018-11-29 14:57:16', '2018-11-29 14:57:16', NULL, NULL, NULL, NULL, 1);

-- Password: Test123$
INSERT INTO `admins` (`id`, `email`, `name`, `active`, `encrypted_password`, `can_read`, `can_execute`, `can_manage`, inserted_at, updated_at)
  VALUES
    (1, 'admin@example.com', 'Admin', 1, '$2a$12$iWHEz.Dd3P6ExPLKu2oCVeCr5L9QM0RrR8fp6vTd6Kz9G39Af2om6', 1, 1, 1, NOW(), NOW());
