#!/usr/bin/env bash

declare -- FA_URL=http://auth.betty.docker
declare -- FA_AUTH_KEY=jEwDLn0iAIDm_yCAF3CGHeKOWEkQumTtI1YjhdSZYVXdheqOOlSWbEQV
declare -- BETTY_BASE_DOMAIN=betty.docker

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'
DONE=" ${GREEN}done${NC}"

cURL () (
  # The --output should write to a log file to help with debugging, furthermore,
  # this file should be dumped upon receiving a non 200 response from this
  # function.
  status=$(curl --write-out "%{http_code}\n" \
       --silent \
       --output /tmp/result \
       --request "$1" \
       --header "Authorization: ${FA_AUTH_KEY}" \
       --header "content-type: application/json" \
       --url "${FA_URL}$2" \
       "${@:3}")

  if [ "$status" = "200" ]; then
    echo -e "$DONE"
  else
    cat /tmp/result
    echo -e "\n${RED}Previous request failed${NC}"
    exit 1
  fi
)

echo "Waiting until fusionauth accepts the API key."

until [[ "`curl -s -o /dev/null -w "%{http_code}" --request PUT --url ${FA_URL}/api/user/search -H "Authorization: ${FA_AUTH_KEY}" | grep -n "200"`" ]]; do
  sleep 0.1;
done;

echo -en "Add builder lambda\t\t\t\t\t\t... "
cURL POST /api/lambda/1f7b37a4-bc38-4f8a-acb9-a3a737adda09 --data '{
  "lambda": {
    "body": "function populate(jwt, user, registration) {jwt.cas_token = user.data.bettyUserId; jwt.user_id = registration.data.remoteUserId; }",
    "debug": false,
    "enabled": true,
    "name": "add cas token and remote user id",
    "type": "JWTPopulate"
  }
}'

echo -en "Add my-bb lambda\t\t\t\t\t\t... "
cURL POST /api/lambda/9f7b37a4-bc38-4f8a-acb9-a3a737adda09 --data '{
  "lambda": {
    "body": "function populate(jwt, user, registration) {jwt.cas_token = user.data.bettyUserId;}",
    "debug": false,
    "enabled": true,
    "name": "add cas token",
    "type": "JWTPopulate"
  }
}'

echo -en "Add new openid reconcile lambda\t\t\t\t\t... "
cURL POST /api/lambda/9549b8f6-162c-4746-a353-ad2479d4213e --data '{
  "lambda": {
    "body": "/* Reconcile lambda from betty that will correctly set some values so users are created in the correct way with a webhook. */\r\n\r\nfunction reconcile(user, registration, jwt) {\r\n  console.debug(JSON.stringify(jwt, null, 2));\r\n  console.debug(JSON.stringify(user, null, 2));\r\n\r\n  if (user.id === undefined) {\r\n    var casToken = SHA1(jwt.email);\r\n\r\n    var p1 = casToken.substr(0, 8);\r\n    var p2 = casToken.substr(8, 4);\r\n    var p3 = casToken.substr(12, 4);\r\n    var p4 = casToken.substr(16, 4);\r\n    var p5 = casToken.substr(20, 12);\r\n\r\n    user.id = p1 + '\''-'\'' + p2 + '\''-'\'' + p3 + '\''-'\'' + p4 + '\''-'\'' + p5;\r\n    user.fullName = jwt.name || jwt.email;\r\n\r\n    user.data.bettyUserId = casToken;\r\n    user.data.oauthUser = true;\r\n  }\r\n  console.debug(JSON.stringify(user, null, 2));\r\n}\r\n\r\nfunction SHA1 (msg) {\r\n\r\n  function rotate_left(n,s) {\r\n    var t4 = ( n<<s ) | (n>>>(32-s));\r\n    return t4;\r\n  }\r\n\r\n  function lsb_hex(val) {\r\n    var str=\"\";\r\n    var i;\r\n    var vh;\r\n    var vl;\r\n\r\n    for( i=0; i<=6; i+=2 ) {\r\n      vh = (val>>>(i*4+4))&0x0f;\r\n      vl = (val>>>(i*4))&0x0f;\r\n      str += vh.toString(16) + vl.toString(16);\r\n    }\r\n    return str;\r\n  }\r\n\r\n  function cvt_hex(val) {\r\n    var str=\"\";\r\n    var i;\r\n    var v;\r\n\r\n    for( i=7; i>=0; i-- ) {\r\n      v = (val>>>(i*4))&0x0f;\r\n      str += v.toString(16);\r\n    }\r\n    return str;\r\n  }\r\n\r\n\r\n  function Utf8Encode(string) {\r\n    string = string.replace(/\\r\\n/g,\"\\n\");\r\n    var utftext = \"\";\r\n\r\n    for (var n = 0; n < string.length; n++) {\r\n\r\n      var c = string.charCodeAt(n);\r\n\r\n      if (c < 128) {\r\n        utftext += String.fromCharCode(c);\r\n      }\r\n      else if((c > 127) && (c < 2048)) {\r\n        utftext += String.fromCharCode((c >> 6) | 192);\r\n        utftext += String.fromCharCode((c & 63) | 128);\r\n      }\r\n      else {\r\n        utftext += String.fromCharCode((c >> 12) | 224);\r\n        utftext += String.fromCharCode(((c >> 6) & 63) | 128);\r\n        utftext += String.fromCharCode((c & 63) | 128);\r\n      }\r\n\r\n    }\r\n\r\n    return utftext;\r\n  }\r\n\r\n  var blockstart;\r\n  var i, j;\r\n  var W = new Array(80);\r\n  var H0 = 0x67452301;\r\n  var H1 = 0xEFCDAB89;\r\n  var H2 = 0x98BADCFE;\r\n  var H3 = 0x10325476;\r\n  var H4 = 0xC3D2E1F0;\r\n  var A, B, C, D, E;\r\n  var temp;\r\n\r\n  msg = Utf8Encode(msg);\r\n\r\n  var msg_len = msg.length;\r\n\r\n  var word_array = [];\r\n  for( i=0; i<msg_len-3; i+=4 ) {\r\n    j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |\r\n      msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);\r\n    word_array.push( j );\r\n  }\r\n\r\n  switch( msg_len % 4 ) {\r\n    case 0:\r\n      i = 0x080000000;\r\n      break;\r\n    case 1:\r\n      i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;\r\n      break;\r\n\r\n    case 2:\r\n      i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;\r\n      break;\r\n\r\n    case 3:\r\n      i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8    | 0x80;\r\n      break;\r\n  }\r\n\r\n  word_array.push( i );\r\n\r\n  while( (word_array.length % 16) != 14 ) word_array.push( 0 );\r\n\r\n  word_array.push( msg_len>>>29 );\r\n  word_array.push( (msg_len<<3)&0x0ffffffff );\r\n\r\n\r\n  for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {\r\n\r\n    for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];\r\n    for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);\r\n\r\n    A = H0;\r\n    B = H1;\r\n    C = H2;\r\n    D = H3;\r\n    E = H4;\r\n\r\n    for( i= 0; i<=19; i++ ) {\r\n      temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;\r\n      E = D;\r\n      D = C;\r\n      C = rotate_left(B,30);\r\n      B = A;\r\n      A = temp;\r\n    }\r\n\r\n    for( i=20; i<=39; i++ ) {\r\n      temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;\r\n      E = D;\r\n      D = C;\r\n      C = rotate_left(B,30);\r\n      B = A;\r\n      A = temp;\r\n    }\r\n\r\n    for( i=40; i<=59; i++ ) {\r\n      temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;\r\n      E = D;\r\n      D = C;\r\n      C = rotate_left(B,30);\r\n      B = A;\r\n      A = temp;\r\n    }\r\n\r\n    for( i=60; i<=79; i++ ) {\r\n      temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;\r\n      E = D;\r\n      D = C;\r\n      C = rotate_left(B,30);\r\n      B = A;\r\n      A = temp;\r\n    }\r\n    H0 = (H0 + A) & 0x0ffffffff;\r\n    H1 = (H1 + B) & 0x0ffffffff;\r\n    H2 = (H2 + C) & 0x0ffffffff;\r\n    H3 = (H3 + D) & 0x0ffffffff;\r\n    H4 = (H4 + E) & 0x0ffffffff;\r\n  }\r\n  var shaValue = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);\r\n  return shaValue.toLowerCase();\r\n}",
    "debug": false,
    "enabled": true,
    "name": "Betty OpenID reconcile lambda",
    "type": "OpenIDReconcile"
  }
}'

echo -en "Add betty templates\t\t\t\t\t\t... "
cURL POST /api/theme/d89c982b-d23d-45b6-9ea5-e34d7561952c --data '@./init.d/fusionauth-theme.json'

echo -en "Add login retry protection\t\t\t\t\t... "
cURL POST /api/user-action/c0afcc27-fa9d-4b30-8eb3-7f343a8fc9ca --data '{
  "userAction": {
    "active": true,
    "name": "LoginProtection",
    "preventLogin": true,
    "sendEndEvent": false,
    "temporal": true
  }
}'

# Extract the tenant ID.
tenant_id=$(
  curl --silent \
       --request GET \
       --url $FA_URL/api/tenant \
       --header "authorization: ${FA_AUTH_KEY}" \
       | sed -n 's/.*id"\:"\([0-9a-f\-]*\)".*/\1/p')

# Add tenant-id to the global docker environment-settings
echo "TENANT_ID=$tenant_id" >> .env

echo -en "Configure tenant\t\t\t\t\t\t... "
cURL PATCH /api/tenant/$tenant_id --data '{
  "tenant": {
    "issuer": "'"${BETTY_BASE_DOMAIN}"'",
      "emailConfiguration": {
      "host": "mailcatcher",
      "port": 1025
    },
    "name": "Betty Blocks",
    "passwordEncryptionConfiguration": {
      "encryptionScheme": "salted-pbkdf2-hmac-sha256",
      "encryptionSchemeFactor": 24000,
      "modifyEncryptionSchemeOnLogin": true
    },
    "failedAuthenticationConfiguration": {
      "actionDuration": 3,
      "actionDurationUnit": "MINUTES",
      "resetCountInSeconds": 60,
      "tooManyAttempts": 5,
      "userActionId": "c0afcc27-fa9d-4b30-8eb3-7f343a8fc9ca"
    },
    "passwordValidationRules": {
      "maxLength": 256,
      "minLength": 8,
      "rememberPreviousPasswords": {
        "enabled": false
      },
      "requireMixedCase": true,
      "requireNonAlpha": true,
      "requireNumber": true,
      "validateOnLogin": false
    },
    "externalIdentifierConfiguration": {
      "changePasswordIdTimeToLiveInSeconds": 1800
    },
    "themeId": "d89c982b-d23d-45b6-9ea5-e34d7561952c"
  }
}'

echo -en "Set environment zone setting\t\t\t\t\t... "
cURL PATCH /api/system-configuration --data '{
  "systemConfiguration":{"reportTimezone":"Europe/Amsterdam"}
}'

source config/synapse_secrets.env
source config/synapse_config.env
echo -en "Add new webhook for processing user login through openid\t... "
cURL POST /api/webhook/36d36046-862f-4894-bba3-4f20b3cf7805 --data '{
  "webhook": {
    "connectTimeout": 5000,
    "description": "Betty webhook that will send messages to synapse about user creation.",
    "eventsEnabled": {
      "user.create": true
    },
    "global": true,
    "readTimeout": 5000,
    "httpAuthenticationUsername": "'"$FUSIONAUTH_BASIC_USERNAME"'",
    "httpAuthenticationPassword": "'"$FUSIONAUTH_BASIC_PASSWORD"'",
    "url": "http://synapse:4001/fusion/webhooks"
  }
}'
