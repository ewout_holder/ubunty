#! /usr/bin/env bash

echo "Bootstrap synapse with application and admin-user"

# Run import
mysql -h 127.0.0.1 -uroot -D synapse_dev < ./init.d/synapse-my-bb-bootstrap-data.sql

docker-compose exec synapse bin/synapse rpc '
app = Synapse.Repo.get_by(Synapse.Schema.Application, %{identifier: "my-bb"})
Synapse.FusionAuth.Application.create(app)
Synapse.FusionAuth.UserImport.import
'

echo "Bootstrap synapse complete"
