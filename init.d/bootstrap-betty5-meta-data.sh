#! /usr/bin/env bash

TABLES=$(mysql -u root --host 127.0.0.1 --database betty5 -e "SHOW TABLES" -sN)
for table in $TABLES; do
  if [[ $table =~ "schema_migrations" ]]; then
    echo "Skip table $table"
  else
    echo "Clean table $table"
    mysql -u root --host 127.0.0.1 --database betty5 -e "SET FOREIGN_KEY_CHECKS=0; truncate table $table; SET FOREIGN_KEY_CHECKS=1;"
  fi
done

mysql -u root --host 127.0.0.1 betty5 < ./init.d/betty5_meta_data.sql
