#! /usr/bin/env bash

(
  cd ..

  docker-compose run betty6 bash -c "rm -rf var && bin/betty create_db && bin/betty migrate"
  docker-compose run replicator bash -c "rm -rf var && bin/replicator create_db && bin/replicator migrate"
  docker-compose run synapse bash -c "rm -rf var && bin/synapse create_db && bin/synapse migrate"
)
